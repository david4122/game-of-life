import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm") version "1.3.72"

    // Apply the application plugin to add support for building a CLI application.
    application

	id("org.openjfx.javafxplugin") version "0.0.9"
}

repositories {
    // Use jcenter for resolving dependencies.
    jcenter()
	mavenCentral()
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	implementation("no.tornado:tornadofx:1.7.20")

    // Use the Kotlin test library.
    testImplementation("org.jetbrains.kotlin:kotlin-test")

    // Use the Kotlin JUnit integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

application {
    // Define the main class for the application.
    mainClassName = "org.gol.AppKt"

	applicationDefaultJvmArgs = listOf("--add-opens", "javafx.graphics/com.sun.glass.ui=ALL-UNNAMED")
}

javafx {
	modules("javafx.controls", "javafx.fxml")
}

tasks.withType<KotlinCompile>().configureEach {
	kotlinOptions.jvmTarget = "11"
}

