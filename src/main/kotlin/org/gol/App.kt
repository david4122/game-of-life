package org.gol

import org.gol.domain.World
import java.util.Arrays

import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min

import org.gol.view.WorldLayout

import tornadofx.*

class GameOfLife: App(WorldLayout::class)

fun main(args: Array<String>) {

	launch<GameOfLife>(args)
}


fun Int.radiusRange(high: Int, low: Int = 0, r: Int = 1): IntRange =
		max(low, this - r)..min(high, this + r)

fun Int.clamp(high: Int, low: Int = 0) = max(low, min(high, this))
