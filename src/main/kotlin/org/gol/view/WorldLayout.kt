package org.gol.view

import tornadofx.*
import javafx.scene.input.KeyEvent
import javafx.scene.input.KeyCode
import javafx.concurrent.Task
import javafx.scene.control.Label
import javafx.scene.control.Button
import javafx.geometry.Pos

import org.gol.domain.*
import org.gol.domain.selector.*
import org.gol.domain.checker.*
import org.gol.domain.counting.*

class WorldLayout: View() {

	val world = CountingWorld(200, 100).detectCycles().cross().checker(1..3, 2)
	val worldView = WorldView(world, 5.0)
	val generation = Label()

	lateinit var runButton: Button

	private var worldTask: Task<Unit>? = null

	override val root = vbox {
		alignment = Pos.CENTER

		keyboard {
			addEventHandler(KeyEvent.KEY_PRESSED) {
				when(it.code) {
					KeyCode.ENTER -> advance()
					KeyCode.SPACE -> run()
					KeyCode.C -> resetWorld()
					else -> return@addEventHandler
				}
			}
		}

		add(generation)

		add(worldView)

		hbox(10, Pos.CENTER) {
			prefHeight = 50.0

			button("Next generation") {
				action {
					advance()
				}
			}

			button("Run") {
				runButton = this

				action {
					run()
				}
			}

			button("Randomize") {
				action {
					world.randomize(0.5, false)
					updateStats()
					worldView.renderWorld()
				}
			}

			button("Reset") {
				action {
					resetWorld()
				}
			}
		}
	}

	init {
		for(i in 0..(world.width-1))
			world[i, world.height/2] = true
		for(i in 0..(world.height-1))
			world[world.width/2, i] = true
		updateStats()
		worldView.renderWorld()
	}

	fun advance() {
		try {
			world.advance()
		} catch(e: CycleDetectedException) {
			println(e.message)
			pause()
		}
		updateStats()
		worldView.renderWorld()
	}

	fun resetWorld() {
		world.reset()
		updateStats()
		worldView.renderWorld()
	}

	fun run() {
		if(worldTask == null) {
			runButton.text = "Pause"
			start()
		} else {
			runButton.text = "Run"
			pause()
		}
	}

	fun updateStats() {
		generation.text = "Generation: ${world.generation}, Population: ${world.population}/${world.size}"
	}

	private fun start() {
		worldTask = runAsync(daemon = true) {
			while(true) {
				Thread.sleep(150L)

				runLater {
					advance()
				}
			}
		}
	}

	private fun pause() {
		worldTask?.cancel()
		worldTask = null
	}
}
