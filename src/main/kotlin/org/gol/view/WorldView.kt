package org.gol.view

import tornadofx.*

import javafx.event.EventHandler
import javafx.scene.input.MouseEvent
import javafx.scene.input.ScrollEvent
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.paint.*
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.StackPane
import javafx.geometry.Pos

import org.gol.*
import org.gol.domain.World

class WorldView(
	val world: World,
	var cellSize: Double = 10.0
): Fragment() {

	val canvas = Canvas(world.width * cellSize, world.height * cellSize).apply {
		addEventFilter(MouseEvent.MOUSE_PRESSED) { e ->
			if(e.isPrimaryButtonDown) {
				world.toggleCell((e.x / cellSize).toInt(), (e.y / cellSize).toInt())
				renderWorld()
			}
		}

		onMouseDragged = object: EventHandler<MouseEvent> {
			lateinit var last: Pair<Int, Int>

			override fun handle(e: MouseEvent) {
				if(e.x < 0 || e.y < 0 || e.x > width || e.y > height)
					return
				val pos = (e.x / cellSize).toInt() to (e.y / cellSize).toInt()
				if(this::last.isInitialized && pos == last)
					return
				last = pos
				world[pos.first.clamp(world.width-1), pos.second.clamp(world.height-1)] = e.isPrimaryButtonDown
				renderWorld()
			}
		}

		onScroll = object: EventHandler<ScrollEvent> {
			override fun handle(e: ScrollEvent) {
				cellSize += e.deltaY / 20
				width = world.width * cellSize
				height = world.height * cellSize
				renderWorld()
			}
		}
	}

	override val root = stackpane {
		style = "-fx-background-color: #111"

		getChildren().add(canvas)
		maxWidthProperty().bind(canvas.widthProperty())
	}

	fun renderWorld() {
		val gcontext = canvas.graphicsContext2D
		gcontext.clearRect(0.0, 0.0, canvas.width, canvas.height)
		world.forEachCell { w, x, y ->
			gcontext.fill = if(w[x, y]) Color.rgb(200, 200, 200) else Color.TRANSPARENT
			gcontext.fillSquare(x * cellSize, y * cellSize, cellSize)
		}
	}
}

fun GraphicsContext.fillSquare(x: Double, y: Double, size: Double) = fillRect(x, y, size, size)
