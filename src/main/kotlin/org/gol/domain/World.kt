package org.gol.domain

interface World {

	interface CellChecker {
		fun check(w: World, x: Int, y: Int): Boolean?
	}

	interface CellSelector {
		fun applyAround(w: World, x: Int, y: Int, f: (Int, Int) -> Unit)
	}

	val width: Int
	val height: Int

	val size: Int
		get() = width * height

	val generation: Int

	val population: Int

	var checker: CellChecker
	var selector: CellSelector

	operator fun get(x: Int, y: Int): Boolean
	operator fun set(x: Int, y: Int, alive: Boolean)

	fun advance()

	fun neighbours(x: Int, y: Int): Int

	fun toggleCell(x: Int, y: Int)

	fun forEachCell(f: (World, Int, Int) -> Unit)

	fun contentHashCode(): Int

	fun reset()
}
