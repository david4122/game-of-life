package org.gol.domain.selector

import org.gol.*
import org.gol.domain.World

class BoundCellSelector: World.CellSelector {

	override fun applyAround(w: World, x: Int, y: Int, f: (Int, Int) -> Unit) {
		for(i in x.radiusRange(w.width - 1)) {
			for(j in y.radiusRange(w.height - 1)) {
				if(i == x && j == y)
					continue
				f(i, j)
			}
		}
	}
}

fun World.bound() = apply {
	selector = BoundCellSelector()
}
