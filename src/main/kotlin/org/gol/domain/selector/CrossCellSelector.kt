package org.gol.domain.selector

import org.gol.domain.World

class CrossCellSelector: World.CellSelector {

	override fun applyAround(w: World, x: Int, y: Int, f: (Int, Int) -> Unit) {
		if(x > 0)
			f(x - 1, y)
		if(x < w.width - 1)
			f(x + 1, y)

		if(y > 0)
			f(x, y - 1)
		if(y < w.height - 1)
			f(x, y + 1)
	}
}

fun World.cross() = apply {
	selector = CrossCellSelector()
}
