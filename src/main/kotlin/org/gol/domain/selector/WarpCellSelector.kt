package org.gol.domain.selector

import org.gol.domain.World

class WarpCellSelector: World.CellSelector {

	override fun applyAround(w: World, x: Int, y: Int, f: (Int, Int) -> Unit) {
		for(i in x-1..x+1) {
			for(j in y-1..y+1) {
				if(i == x && j == y)
					continue
				f(i.loop(w.width), j.loop(w.height))
			}
		}
	}

	private fun Int.loop(n: Int) = (n + this) % n
}

fun World.warped() = apply {
	selector = WarpCellSelector()
}
