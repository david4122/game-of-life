package org.gol.domain.counting

import kotlin.math.max
import kotlin.math.min

import kotlin.random.Random

import org.gol.domain.checker.DefaultCellChecker
import org.gol.domain.selector.WarpCellSelector
import org.gol.domain.World

typealias WorldArray = Array<Array<MutableCell>>

open class CountingWorld private constructor(
	private var world: WorldArray,
	override var checker: World.CellChecker,
	override var selector: World.CellSelector
): World {

	constructor(width: Int, height: Int,
		checker: World.CellChecker = DefaultCellChecker(),
		selector: World.CellSelector = WarpCellSelector()
	): this(WorldArray(width) { Array<MutableCell>(height) { MutableCell() } },
		checker, selector)

	constructor(size: Int,
		checker: World.CellChecker = DefaultCellChecker(),
		selector: World.CellSelector = WarpCellSelector()
	): this(size, size, checker, selector)



	override val width
		get() = world.size
	override val height
		get() = world[0].size

	override val size
		get() = width * height

	override var generation = 1
		protected set

	override var population = 0
		protected set

	private var next = WorldArray(width) { x ->
		Array<MutableCell>(height) { y ->
			world[x][y].copy()
		}
	}


	override operator fun get(x: Int, y: Int) = world[x][y].alive
	override operator fun set(x: Int, y: Int, alive: Boolean) {
		if(world.setAlive(x, y, alive)) {
			next.setAlive(x, y, alive)

			if(alive) population++ else population--
		}
	}


	override fun advance() {
		world.forEachCell { _, x, y ->
			checker.check(this, x, y)?.let {
				next.setAlive(x, y, it)
			}
		}

		population = 0
		next.forEachCell { c, x, y ->
			c.copyTo(world[x][y])
			if(c.alive)
				population++
		}

		generation++
	}

	override fun neighbours(x: Int, y: Int): Int = world[x][y].neighbours

	override fun forEachCell(f: (World, Int, Int) -> Unit) {
		world.forEachCell { _, x, y -> f(this, x, y) }
	}

	override fun toggleCell(x: Int, y: Int) {
		this[x, y] = !this[x, y]
	}

	override fun contentHashCode(): Int {
		var hash = 0
		world.forEachCell { c, x, y ->
			hash = 31 * hash + x
			hash = 31 * hash + y
			hash = 31 * hash + c.hashCode()
		}
		return hash
	}

	override fun reset() {
		world.forEachCell { c, x, y ->
			c.alive = false
			c.neighbours = 0

			next[x][y].alive = false
			next[x][y].neighbours = 0
		}
		population = 0
		generation = 0
	}


	private fun WorldArray.setAlive(x: Int, y: Int, alive: Boolean): Boolean {
		if(this[x][y].alive != alive) {
			this[x][y].alive = alive

			if(alive) {
				selector.applyAround(this@CountingWorld, x, y) { a, b -> this[a][b].neighbours++ }
			} else {
				selector.applyAround(this@CountingWorld, x, y) { a, b -> this[a][b].neighbours-- }
			}

			return true
		}
		return false
	}

	private fun WorldArray.forEachCell(f: (MutableCell, Int, Int) -> Unit) {
		repeat(width) { i ->
			repeat(height) { j ->
				f(this[i][j], i, j)
			}
		}
	}
}

fun World.randomize(perc: Double, exact: Boolean = true) {
	reset()
	repeat((size * perc).toInt()) {
		var x: Int
		var y: Int

		do {
			x = Random.nextInt(width)
			y = Random.nextInt(height)
		} while(exact && this[x, y])
		this[x, y] = true
	}
}
