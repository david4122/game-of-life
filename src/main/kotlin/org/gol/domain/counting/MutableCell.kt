package org.gol.domain.counting

class MutableCell(
	override var alive: Boolean = false,
	override var neighbours: Int = 0
): Cell(alive, neighbours) {

	override fun copy() = MutableCell(alive, neighbours)

	fun copyTo(c: MutableCell): Cell {
		c.alive = alive
		c.neighbours = neighbours
		return c
	}
}
