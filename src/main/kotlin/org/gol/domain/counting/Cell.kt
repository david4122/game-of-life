package org.gol.domain.counting

open class Cell(
	open val alive: Boolean = false,
	open val neighbours: Int = 0
) {

	open fun copy() = Cell(alive, neighbours)

	override fun toString() = "${if(alive) "X" else "."}($neighbours)"

	override fun hashCode(): Int {
		var hash = if(alive) 0 else 1
		hash = 37 * hash + neighbours
		hash = 37 * hash + super.hashCode()
		return hash
	}
}
