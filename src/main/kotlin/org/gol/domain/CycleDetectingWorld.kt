package org.gol.domain

class CycleDetectedException(val current: Int, val firstOccurence: Int): Exception("Cycle detected between $firstOccurence and $current, length ${current - firstOccurence}")

class CycleDetectingWorld(val base: World): World by base {

	private val _past = HashMap<Int, Int>()
	val past: Map<Int, Int>
		get() = _past

	override fun advance() {
		if(population > 0) {
			base.advance()
			val hashCode = contentHashCode()
			val firstOccurence = _past.getOrPut(hashCode) { generation }
			if(firstOccurence != generation) {
				throw CycleDetectedException(generation, firstOccurence)
			}
		}
	}

	override fun reset() {
		_past.clear()
		base.reset()
	}
}

fun World.detectCycles(): CycleDetectingWorld = CycleDetectingWorld(this)
