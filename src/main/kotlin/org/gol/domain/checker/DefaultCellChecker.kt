package org.gol.domain.checker

import org.gol.domain.World

class DefaultCellChecker(
	val livingZone: IntRange = 2..3,
	val reproduce: Int = 3
): World.CellChecker {

	override fun check(w: World, x: Int, y: Int): Boolean? {
		if(w[x, y]) {
			if(w.neighbours(x, y) !in livingZone)
			return false
		} else {
			if(w.neighbours(x, y) == reproduce)
			return true
		}
		return null
	}
}

fun World.checker(ok: IntRange = 2..3, r: Int = 3) = apply {
	checker = DefaultCellChecker(ok, r)
}
